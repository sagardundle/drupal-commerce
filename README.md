# E-commerce website using drupal 8.9

## Home Screenshot
![picture](images/home.PNG)

## sidebar screenshot
![picture](images/sidebar.PNG)

## Footer Screenshot
![picture](images/footer.PNG)

## Product Details page
![picture](images/details-page.PNG)

## Product hover page
![picture](images/product-hover.png)

## Product add
![picture](images/product-add.PNG)

## Shopping bag
![picture](images/shopping-bag.PNG)

## Cart
![picture](images/cart.PNG)

## Login
![picture](images/login.PNG)

## Checkout Step 1
![picture](images/checkout1.PNG)

## Checkout Step 2
![picture](images/checkout2.PNG)

## Checkout Step 3
![picture](images/checkout3.PNG)

## Order review
![picture](images/review.PNG)

## Order Confirmation
![picture](images/confirmation.PNG)

## Registration
![picture](images/registration.PNG)

## User view own Orders
![picture](images/user-login-orders.PNG)

## Admin view all orders
![picture](images/admin-login-orders.PNG)

## Add Product
![picture](images/add-product.PNG)
![picture](images/add-product1.PNG)