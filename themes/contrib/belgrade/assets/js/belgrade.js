/**
 * @file
 * Belgrade Theme JS.
 */
(function ($) {

    'use strict';

    /**
     * Close behaviour.
     */
    Drupal.behaviors.closeCartblockcontents = {
      attach: function (context, settings) {
        $('.cart-block--contents .close-btn').click(function() {
          $(this).parent().removeClass('cart-block--contents__expanded');
        });
      }
    };

    $('.form-item-field-mobile-no-0-value>input').keypress(function(event){

      if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
          event.preventDefault(); //stop character from entering input
      }
      var length = jQuery(this).val().length;
       if(length > 9) {
            return false;
       } else if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
       } else if((length == 0) && (e.which == 48)) {
            return false;
       }

    });

  })(jQuery);
