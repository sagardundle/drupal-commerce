<?php

namespace Drupal\cart\Controller;

use Drupal\Core\Controller\ControllerBase;

/* 
    To get taxonomy term wise products
    @return build
*/
class FilterTaxonomy extends ControllerBase{
    public function get()
    {
        $product_view_builder = \Drupal::entityTypeManager()->getViewBuilder('commerce_product');
        $tid = \Drupal::routeMatch()->getParameter('tid');
        $build = [
            '#theme' => 'taxonomy',
          ];
        
        $title = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid)->get('name')->value;

        $product_storage = \Drupal::entityTypeManager()->getStorage('commerce_product');
        
        $product_ids  = \Drupal::entityQuery('commerce_product')
                            ->condition('field_select_a_category.entity.tid', $tid)
                            ->execute();
        
        $featured_products = $product_storage->loadMultiple($product_ids);
        foreach ($featured_products as $product) {            
            $build['#products'][] = $product_view_builder->view($product, 'catalog');
        }
            $build['#title'] = $title;
        return $build;

    }
}